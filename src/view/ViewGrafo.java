package view;

import java.awt.Graphics;
import java.io.File;
import java.util.Arrays;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import jgraph.Arista;
import jgraph.JGrafo;
import jgraph.Nodo;
import jgraph.Ruta;
import main.Apoyo;
import main.ZoneEvent;
import pointedlist.PointedList;

/**
 *
 * @author Leonardo
 */
public class ViewGrafo extends javax.swing.JFrame {
    
    private ViewStart vs;
    private String directory;
    
    public ViewGrafo(jgraph.JGrafo jGrafo, String directory, ViewStart vs) {
        this.vs=vs;
        this.directory=directory;
        this.jGrafo=jGrafo;
        jGrafo.viewGrafo=this;
        initComponents();
        jGrafo.up();
        initComboBoxes();
        
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(false);
        jGrafo.requestFocus();
        labelIteración.setText("Iteración #"+jGrafo.getIteración());
        initFileChooser();
        dialog.setSize(580, 450);
        dialog.setResizable(false);
        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    private void initFileChooser(){
        chooser.setDialogType(JFileChooser.SAVE_DIALOG);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setCurrentDirectory(new File(directory));
        directory=chooser.getCurrentDirectory().getAbsolutePath();
    }
    
    public void showRutaContagios(PointedList<Ruta> lista, Nodo nodo){
        labelDialogTitle.setText("RUTAS DE POSIBLES CONTAGIOS");
        labelDialogInfo.setText("DATOS DEL INDIVIDUO");
        labelDialogId.setText("Id: "+nodo.getId());
        labelDialogMask.setText("Mascarilla: "+(nodo.getPersona().hasMask()?"Sí":"No"));
        labelDialogCont.setText("Contagiado: "+(nodo.getPersona().isContagiado()?"Sí":"No"));
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        areaInfo.setText("");
        if(lista.isEmpty()){
            areaInfo.setText("No hay rutas de contagio para este nodo.\nRegresar al rato.");
            return;
        }
        
        Ruta[] rutas=tortasDePapaya(lista);
        Arrays.sort(rutas);
        for(Ruta r: rutas){
            Nodo[] nodos=r.toNodosVector();
            for(int i=0; i<nodos.length; i++){
                areaInfo.append(nodos[i].getId()+"");
                if(i!=nodos.length-1) areaInfo.append(" <- ");
            }
            areaInfo.append("\nProbabilidad de contagio = "+Apoyo.round(r.probContagio(), 2)+"%\n\n");
        }
    }
    
    private Ruta[] tortasDePapaya(PointedList<Ruta> lista){
        Ruta[] rutas=new Ruta[lista.size()];
        int i=0;
        for(Object o: lista)
            rutas[i++]=(Ruta)o;
        
        return rutas;
    }
    
    public void showPotContagios(PointedList<Arista> lista, Nodo nodo){
        labelDialogTitle.setText("POTENCIALES CONTAGIOS");
        labelDialogInfo.setText("DATOS DEL INDIVIDUO");
        labelDialogId.setText("Id: "+nodo.getId());
        labelDialogMask.setText("Mascarilla: "+(nodo.getPersona().hasMask()?"Sí":"No"));
        labelDialogCont.setText("Contagiado: "+(nodo.getPersona().isContagiado()?"Sí":"No"));
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        areaInfo.setText("");
        if(lista.isEmpty()){
            areaInfo.setText("No hay potenciales contagios actualmente.\nRegresar al rato.");
            return;
        }
        
        Arista[] aristas=asereje(lista);
        Arrays.sort(aristas);
        for(Arista a: aristas){
            Nodo n=a.getExtremo1().equals(nodo)?a.getExtremo2():a.getExtremo1();
            areaInfo.append(nodo.getId()+" -> "+n.getId());
            areaInfo.append("\nProbabilidad de contagio = "+Apoyo.round(a.probContagio(), 2)+"%\n\n");
        }
    }
    
    private Arista[] asereje(PointedList<Arista> lista){
        Arista[] aristas=new Arista[lista.size()];
        int i=0;
        for(Object o: lista)
            aristas[i++]=(Arista)o;
        
        return aristas;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chooser = new javax.swing.JFileChooser();
        dialog = new javax.swing.JDialog();
        panelDialog = new javax.swing.JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                g.setColor(new java.awt.Color(51, 51, 51));
                g.fillRect(0, 0, getWidth(), 55);
            }
        };
        labelDialogTitle = new javax.swing.JLabel();
        labelDialogInfo = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        areaInfo = new javax.swing.JTextArea();
        labelDialogCont = new javax.swing.JLabel();
        labelDialogId = new javax.swing.JLabel();
        labelDialogMask = new javax.swing.JLabel();
        panelOptions = new javax.swing.JPanel();
        buttonIterar = new javax.swing.JButton();
        buttonReorder = new javax.swing.JButton();
        buttonRegen = new javax.swing.JButton();
        labelMask = new javax.swing.JLabel();
        labelGenerar = new javax.swing.JLabel();
        buttonGen = new javax.swing.JButton();
        buttonSave = new javax.swing.JButton();
        labelParámteros = new javax.swing.JLabel();
        labelSimulación = new javax.swing.JLabel();
        comboAristas = new javax.swing.JComboBox<>();
        labelAristas = new javax.swing.JLabel();
        labelIteración = new javax.swing.JLabel();
        comboMask = new javax.swing.JComboBox<>();
        jGrafo = jGrafo;
        panelTitle = new javax.swing.JPanel(){
            @Override
            public void paintComponent(Graphics g){
                super.paintComponent(g);
                updatePanelTitle(g);
            }
        };
        labelTitle = new javax.swing.JLabel();

        panelDialog.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelDialogTitle.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 24)); // NOI18N
        labelDialogTitle.setForeground(new java.awt.Color(255, 255, 255));
        labelDialogTitle.setText("RUTAS DE POSIBLES CONTAGIOS");
        panelDialog.add(labelDialogTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 10, -1, -1));

        labelDialogInfo.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelDialogInfo.setForeground(new java.awt.Color(0, 0, 0));
        labelDialogInfo.setText("DATOS DEL INDIVIDUO");
        panelDialog.add(labelDialogInfo, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, -1, -1));

        areaInfo.setEditable(false);
        areaInfo.setColumns(20);
        areaInfo.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        areaInfo.setRows(5);
        areaInfo.setOpaque(false);
        jScrollPane1.setViewportView(areaInfo);

        jScrollPane2.setViewportView(jScrollPane1);

        panelDialog.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 510, 240));

        labelDialogCont.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelDialogCont.setForeground(new java.awt.Color(0, 0, 0));
        labelDialogCont.setText("Contagiado:");
        panelDialog.add(labelDialogCont, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, -1, -1));

        labelDialogId.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelDialogId.setForeground(new java.awt.Color(0, 0, 0));
        labelDialogId.setText("Id:");
        panelDialog.add(labelDialogId, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 100, -1, -1));

        labelDialogMask.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelDialogMask.setForeground(new java.awt.Color(0, 0, 0));
        labelDialogMask.setText("Mascarilla:");
        panelDialog.add(labelDialogMask, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 100, -1, -1));

        javax.swing.GroupLayout dialogLayout = new javax.swing.GroupLayout(dialog.getContentPane());
        dialog.getContentPane().setLayout(dialogLayout);
        dialogLayout.setHorizontalGroup(
            dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogLayout.createSequentialGroup()
                .addComponent(panelDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        dialogLayout.setVerticalGroup(
            dialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogLayout.createSequentialGroup()
                .addComponent(panelDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelOptions.setBackground(new java.awt.Color(255, 153, 153));
        panelOptions.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        buttonIterar.setBackground(new java.awt.Color(51, 51, 51));
        buttonIterar.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        buttonIterar.setForeground(new java.awt.Color(255, 255, 255));
        buttonIterar.setText("Siguiente Iteración");
        buttonIterar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonIterarActionPerformed(evt);
            }
        });
        panelOptions.add(buttonIterar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        buttonReorder.setBackground(new java.awt.Color(51, 51, 51));
        buttonReorder.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        buttonReorder.setForeground(new java.awt.Color(255, 255, 255));
        buttonReorder.setText("Reordenar");
        buttonReorder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonReorderActionPerformed(evt);
            }
        });
        panelOptions.add(buttonReorder, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));

        buttonRegen.setBackground(new java.awt.Color(51, 51, 51));
        buttonRegen.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        buttonRegen.setForeground(new java.awt.Color(255, 255, 255));
        buttonRegen.setText("Generar Grafo (Mismos Parámetros)");
        buttonRegen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRegenActionPerformed(evt);
            }
        });
        panelOptions.add(buttonRegen, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, -1, -1));

        labelMask.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 14)); // NOI18N
        labelMask.setForeground(new java.awt.Color(0, 0, 0));
        labelMask.setText("Mascarilla");
        panelOptions.add(labelMask, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, -1));

        labelGenerar.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelGenerar.setForeground(new java.awt.Color(255, 255, 255));
        labelGenerar.setText("GENERAR");
        panelOptions.add(labelGenerar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, -1));

        buttonGen.setBackground(new java.awt.Color(51, 51, 51));
        buttonGen.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        buttonGen.setForeground(new java.awt.Color(255, 255, 255));
        buttonGen.setText("Generar Nuevo Grafo");
        buttonGen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGenActionPerformed(evt);
            }
        });
        panelOptions.add(buttonGen, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, -1, -1));

        buttonSave.setBackground(new java.awt.Color(51, 51, 51));
        buttonSave.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        buttonSave.setForeground(new java.awt.Color(255, 255, 255));
        buttonSave.setText("Exportar Grafo");
        buttonSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSaveActionPerformed(evt);
            }
        });
        panelOptions.add(buttonSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, -1, -1));

        labelParámteros.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelParámteros.setForeground(new java.awt.Color(255, 255, 255));
        labelParámteros.setText("PARÁMETROS");
        panelOptions.add(labelParámteros, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, -1, -1));

        labelSimulación.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 18)); // NOI18N
        labelSimulación.setForeground(new java.awt.Color(255, 255, 255));
        labelSimulación.setText("SIMULACIÓN");
        panelOptions.add(labelSimulación, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        comboAristas.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        comboAristas.setForeground(new java.awt.Color(0, 0, 0));
        panelOptions.add(comboAristas, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 240, 120, -1));

        labelAristas.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 14)); // NOI18N
        labelAristas.setForeground(new java.awt.Color(0, 0, 0));
        labelAristas.setText("Relaciones");
        panelOptions.add(labelAristas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, -1, -1));

        labelIteración.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 14)); // NOI18N
        labelIteración.setForeground(new java.awt.Color(0, 0, 0));
        labelIteración.setText("Iteración #n");
        panelOptions.add(labelIteración, new org.netbeans.lib.awtextra.AbsoluteConstraints(175, 55, -1, -1));

        comboMask.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 12)); // NOI18N
        comboMask.setForeground(new java.awt.Color(0, 0, 0));
        panelOptions.add(comboMask, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 120, -1));

        panelTitle.setBackground(new java.awt.Color(51, 51, 51));
        panelTitle.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        labelTitle.setFont(new java.awt.Font("Nirmala UI Semilight", 1, 24)); // NOI18N
        labelTitle.setForeground(new java.awt.Color(255, 255, 255));
        labelTitle.setText("THE COVID JOURNEY");
        panelTitle.add(labelTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jGrafo, javax.swing.GroupLayout.DEFAULT_SIZE, 717, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelOptions, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(panelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelTitle, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jGrafo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelOptions, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void initComboBoxes(){
        comboMask.addItem("Sí");
        comboMask.addItem("No");
        comboMask.addItem("Aleatorio");
        comboAristas.addItem("Unidireccionales");
        comboAristas.addItem("Bidireccionales");
        comboAristas.addItem("Aleatorias");
        switch(jGrafo.getTypeMask()){
            case JGrafo.NO_MASK: comboMask.setSelectedIndex(1); break;
            case JGrafo.RANDOM_MASK: comboMask.setSelectedIndex(2);
        }
        switch(jGrafo.getTypeAristas()){
            case JGrafo.ARISTAS_NO_DIRIGIDAS: comboAristas.setSelectedIndex(1); break;
            case JGrafo.RANDOM_ARISTAS: comboAristas.setSelectedIndex(2);
        }
    }
    
    private void updatePanelTitle(Graphics g){
        ZoneEvent ze;
        g.drawImage(Apoyo.back, 10, 10, 55, 30, null);
        ze=new ZoneEvent(10, 10, 55, 30){
            @Override
            public void respuesta(java.awt.event.MouseEvent me){
                dispose();
                vs.backToParameters();
            }
        };
        try{panelTitle.add(ze);
        }catch(Exception e){
            System.out.println("::'v");
        }
    }
    
    private void buttonIterarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonIterarActionPerformed
        if(!jGrafo.hasSanos()) return;
        jGrafo.iterar();
        jGrafo.repaint();
        //jGrafo1.up();
        labelIteración.setText("Iteración #"+jGrafo.getIteración());
    }//GEN-LAST:event_buttonIterarActionPerformed

    private void buttonReorderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonReorderActionPerformed
        jGrafo.up();
    }//GEN-LAST:event_buttonReorderActionPerformed

    private void buttonRegenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRegenActionPerformed
        jGrafo.regen(jGrafo.getNumNodos());
        labelIteración.setText("Iteración #"+jGrafo.getIteración());
    }//GEN-LAST:event_buttonRegenActionPerformed

    private void buttonGenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGenActionPerformed
        dispose();
        vs.backToParameters();
    }//GEN-LAST:event_buttonGenActionPerformed

    private void buttonSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSaveActionPerformed
        JOptionPane.showMessageDialog(this, "En desarrollo...");
        /*
        System.out.println("**ce guarda el archivo uwu**");
        if(chooser.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){
            File f=chooser.getSelectedFile();
            if(!f.exists()){
                System.out.println("fuck up");
                return;
            }
            try{
                FileWriter fw=new FileWriter(f);
                fw.write("jsjsjs");
                fw.close();
            }catch(IOException e){
                System.out.println(e);
            }
        }*/
    }//GEN-LAST:event_buttonSaveActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea areaInfo;
    private javax.swing.JButton buttonGen;
    private javax.swing.JButton buttonIterar;
    private javax.swing.JButton buttonRegen;
    private javax.swing.JButton buttonReorder;
    private javax.swing.JButton buttonSave;
    private javax.swing.JFileChooser chooser;
    private javax.swing.JComboBox<String> comboAristas;
    private javax.swing.JComboBox<String> comboMask;
    private javax.swing.JDialog dialog;
    private jgraph.JGrafo jGrafo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelAristas;
    private javax.swing.JLabel labelDialogCont;
    private javax.swing.JLabel labelDialogId;
    private javax.swing.JLabel labelDialogInfo;
    private javax.swing.JLabel labelDialogMask;
    private javax.swing.JLabel labelDialogTitle;
    private javax.swing.JLabel labelGenerar;
    private javax.swing.JLabel labelIteración;
    private javax.swing.JLabel labelMask;
    private javax.swing.JLabel labelParámteros;
    private javax.swing.JLabel labelSimulación;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JPanel panelDialog;
    private javax.swing.JPanel panelOptions;
    private javax.swing.JPanel panelTitle;
    // End of variables declaration//GEN-END:variables
}
